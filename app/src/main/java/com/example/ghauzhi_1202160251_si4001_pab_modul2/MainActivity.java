package com.example.ghauzhi_1202160251_si4001_pab_modul2;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    DatePickerDialog tanggal;
    TimePickerDialog time;
    SimpleDateFormat format;
    Spinner spinner;
    TextView txttopup, txtsaldo, txttanggal, txtwaktu, txttanggal2, txtwaktu2;
    AlertDialog.Builder tampil;
    LayoutInflater layout;
    View view;
    String saldohasil;
    EditText edtsaldo;

    private String[] list_tujuan = {"Jakarta", "Cirebon", "Bekasi"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /** txtsaldo = (TextView) findViewById(R.id.txt_saldo);

        txttopup = (TextView) findViewById(R.id.txt_topup);
        txttopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtsaldo.setText(null);
                Form();
            }
        }); **/

        spinner = (Spinner) findViewById(R.id.spinner_tujuan);
        ArrayAdapter<String> tujuanAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_tujuan);
        spinner.setAdapter(tujuanAdapter);
        spinner.setOnItemSelectedListener(this);

        format = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        txttanggal = (TextView) findViewById(R.id.txt_tanggal);
        txttanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

        txtwaktu = (TextView) findViewById(R.id.txt_waktu);
        txtwaktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimeDialog();
            }
        });

        txttanggal2 = (TextView) findViewById(R.id.txt_tanggal2);
        txttanggal2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog2();
            }
        });

        txtwaktu2 = (TextView) findViewById(R.id.txt_waktu2);
        txtwaktu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog2();
            }
        });

    }

    private void showDateDialog() {
        Calendar newCalendar = Calendar.getInstance();

        tanggal = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                txttanggal.setText(format.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        tanggal.show();
    }

    private void showTimeDialog() {
        Calendar calendar = Calendar.getInstance();

        time = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                txtwaktu.setText(hourOfDay + ":" + minute);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);

        time.show();
    }

    private void showDateDialog2() {
        Calendar newCalendar = Calendar.getInstance();

        tanggal = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                txttanggal2.setText(format.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        tanggal.show();
    }

    private void showTimeDialog2() {
        Calendar calendar = Calendar.getInstance();

        time = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                txtwaktu2.setText(hourOfDay + ":" + minute);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);

        time.show();
    }

    /**private void kosong() {
        edtsaldo.setText(null);
    }
    private void Form() {
        tampil = new AlertDialog.Builder(MainActivity.this);
        layout = getLayoutInflater();
        view = layout.inflate(R.layout.dialogview, null);
        tampil.setView(view);
        tampil.setCancelable(true);
        tampil.setTitle("Isikan Jumlah Saldo Anda: ");

        edtsaldo = view.findViewById(R.id.txt_saldo);

        kosong();

        tampil.setPositiveButton("Tambah Saldo", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface tampil, int which) {
                saldohasil = edtsaldo.getText().toString();
                txtsaldo.setText("Rp " + saldohasil);
                Toast.makeText(MainActivity.this, "Selamat Top up Anda berhasil", Toast.LENGTH_LONG).show();
                tampil.dismiss();
            }
        });

        tampil.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface tampil, int which) {
                tampil.dismiss();
            }
        });

        tampil.show();
    }
**/
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
